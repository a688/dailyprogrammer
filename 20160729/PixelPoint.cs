﻿namespace _20160729
{
    public struct PixelPoint
    {
        public float x;

        public int MainX;

        public float y;

        public int MainY;

        public PixelPoint(int x, int y)
        {
            this.x = x;
            this.y = y;

            this.MainX = x;
            this.MainY = y;
        }
    }
}
