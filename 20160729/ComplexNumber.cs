﻿using System;

namespace _20160729
{
    public sealed class ComplexNumber
    {
        float real, imaginary;

        public float absolute
        {
            get
            {
                return (float)Math.Sqrt(real * real + imaginary * imaginary);
            }
        }
        
        public ComplexNumber(float real, float imaginary)
        {
            this.real = real;
            this.imaginary = imaginary;
        }

        static ComplexNumber ApplyFunction(ComplexNumber number, ComplexNumber function)
        {
            float r = (number.real * number.real) - (number.imaginary * number.imaginary) + function.real;
            float i = (2 * number.real * number.imaginary) + function.imaginary;
            return new ComplexNumber(r, i);
        }

        public static int countIterations(ComplexNumber number, ComplexNumber function, int maxIterations)
        {
            if (maxIterations <= 0) { return 0; }

            var value = ApplyFunction(number, function);
            if (value.absolute < 2)
            {
                return 1 + countIterations(value, function, maxIterations - 1);
            }

            return 0;
        }
    }
}
