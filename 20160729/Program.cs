﻿using System;
using System.Threading.Tasks;

namespace _20160729
{
    class Program
    {
        static ComplexNumber function = new ComplexNumber(-0.221f, -0.713f);

        static int maxIterations = 255;

        static void Main(string[] args)
        {
            var w = 1920;
            var h = 1080;

            var timer = System.Diagnostics.Stopwatch.StartNew();

            var array = new System.Drawing.Color[w,h];

            var toProcess = new PixelPoint[w * h];
            var idx = 0;
            for (var x = 0; x < w; x++)
            {
                for (var y = 0; y < h; y++)
                {
                    toProcess[idx] = new PixelPoint(x, y);
                    idx++;
                }
            }

            Parallel.For(0, toProcess.Length, z =>
            {
                var cur = new ComplexNumber((toProcess[z].x * 2) / w - 1, (toProcess[z].y * 2) / h - 1);
                var intensity = ComplexNumber.countIterations(cur, function, maxIterations);
                array[toProcess[z].MainX, toProcess[z].MainY] = GetColor(intensity);
            });

            timer.Stop();
            System.Console.WriteLine("Time to calculate factual: " + timer.Elapsed.TotalSeconds.ToString());

            timer.Restart();
            var image = new System.Drawing.Bitmap(w, h);
            for (var x = 0; x < image.Width; x++)
            {
                for (var y = 0; y < image.Height; y++)
                {
                    image.SetPixel(x, y, array[x,y]);
                }
            }
            image.Save(@"c:\Temp\Fractual.png", System.Drawing.Imaging.ImageFormat.Png);

            timer.Stop();
            System.Console.WriteLine("Time to draw image: " + timer.Elapsed.TotalSeconds.ToString());

            Console.ReadLine();
        }

        static System.Drawing.Color GetColor(int intensity)
        {
            if (intensity < 10) { return System.Drawing.Color.White; }
            if (intensity < 20) { return System.Drawing.Color.Lime; }
            if (intensity < 30) { return System.Drawing.Color.LimeGreen; }
            if (intensity < 40) { return System.Drawing.Color.Gray; }

            if (intensity < 100) { return System.Drawing.Color.Violet; }
            if (intensity < 120) { return System.Drawing.Color.Tomato; }

            return System.Drawing.Color.Black;
        }
    }
}