﻿open Expecto

let tests =
    test "A simple test" {
        let subject = "Hello world"
        Expect.equal subject "Hello world" "The strings should equal"
    }

let simpleTest =
    testCase "A simple test" <| fun() ->
        let expected = 4
        Expect.equal expected (2+2) "2+2 4"

[<EntryPoint>]
let main argv = 
    runTestsWithArgs defaultConfig argv tests
    runTests defaultConfig simpleTest

    
    0 // return an integer exit code
