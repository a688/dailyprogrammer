﻿type Ans = { YC : string; XC : string; Bounces : int; Time : int }

// Greatest Common Divisor (Eucludian). A needs to be > than b.
let rec gcd a b = 
    match b with
    | x when x = 0 -> a
    | _ -> gcd b (a % b)

let printAnswer answer =
    printfn "Corner = %s%s\t# Bounces = %i\tTime = %i" answer.YC answer.XC answer.Bounces answer.Time

// Least (/Lowest/Smallest) Common Multiple
let lcm a b = (a * b) / (gcd a b)

let sideCount distanceTraveled size =
    ((distanceTraveled / size) - 1) % 2

let whichSide distanceTraveled size (sides : array<string>) =
    sides.[sideCount distanceTraveled size]

let calculateCorner h w v =
    let distanceTraveled = lcm h w
    let bounces = (distanceTraveled / h) + (distanceTraveled / w) - 2
    let tORb = (whichSide distanceTraveled h [| "L"; "U" |])
    let lORr = (whichSide distanceTraveled w [| "R"; "L" |])
    let time = distanceTraveled / v

    { YC = tORb; XC = lORr; Bounces = bounces; Time = time }

let calculateRectangleCorner h w (m : int) (n : int) v =
    calculateCorner (h - m) (w - n) v

[<EntryPoint>]
let main argv = 
    try
        printAnswer (calculateCorner 8 3 1)
        printAnswer (calculateCorner 15 4 2)

        printAnswer (calculateRectangleCorner 10 7 3 2 1) // (10 * (3 / 2) * 10) (14 / (2 / 2) * 10) 1 20)
    with
    | ex -> printfn "Error: %s\r\n%s" ex.Message ex.StackTrace

    0 // return an integer exit code
