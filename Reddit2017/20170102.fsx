﻿let findGroups value = 
    let rec keepGoing remaining (idx : int) (groupStarts : list<int>) (fullGroups : list<int*int>) =
        match remaining with
        | [] -> fullGroups
        | h::t -> 
            match h with
            | s when s = '(' -> keepGoing t (idx + 1) (idx :: groupStarts) fullGroups
            | s when s = ')' -> keepGoing t (idx + 1) (groupStarts.Tail) ((groupStarts.Head, idx) :: fullGroups)
            | _ -> keepGoing t (idx + 1) groupStarts fullGroups

    keepGoing (value |> Seq.toList) 0 [] []

let doWork (value : string) = 
    let betterStart = value.Replace("()", "")

    let output = 
        match betterStart.Length = 0 with
        | true -> "NULL"
        | false -> 
            let groups = 
                (findGroups betterStart)
                |> List.toArray

            let toRemove = 
                groups 
                |> Array.filter(fun (b, e) -> (groups |> Array.contains(b - 1, e + 1)))
                |> Array.map(fun (b, e) -> [| b; e |])
                |> Array.collect(id)
        
            betterStart 
            |> Seq.mapi(fun i c -> 
                match (toRemove |> Array.contains i) with
                | true -> None
                | false -> Some(c.ToString())
                )
            |> Seq.choose(id)
            |> Seq.map(string)
            |> Seq.reduce(+)

    sprintf "%s -> %s" value output

printfn "%s" (doWork "((a((bc)(de)))f)")
printfn "%s" (doWork "(((zbcd)(((e)fg))))")
printfn "%s" (doWork "ab((c))")

printfn "%s" (doWork "()")
printfn "%s" (doWork "((fgh()()()))")
printfn "%s" (doWork "()(abc())")
