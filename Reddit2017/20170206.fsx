﻿type Element(name : string, symbol : string, z : int, weight : double, c : option<double>) =
    member x.Name = name
    member x.Symbol = symbol
    member x.Z = z
    member x.Weight = weight
    member x.c = c

let parseWeight w =
    let regMatch = System.Text.RegularExpressions.Regex.Match(w, @"[0-9]+\.{0,1}[0-9]*?")
    System.Convert.ToDouble(regMatch.Value)

let parseC c =
    match System.String.IsNullOrWhiteSpace(c) with
    | true -> None
    | false -> Some(System.Convert.ToDouble(c))

let getElements filePath = 
    System.IO.File.ReadAllLines(filePath)
    |> Array.map (fun l -> l.Split([| ',' |], System.StringSplitOptions.None))
    |> Array.map (fun a -> 
                        Element(
                            a.[0], 
                            a.[1], 
                            (System.Convert.ToInt32(a.[2])), 
                            (parseWeight a.[3]), 
                            (parseC a.[4])
                            )
                )

let mapElements (elements : array<Element>) =
    elements |> Array.map(fun e -> (e.Symbol.ToUpperInvariant(), e)) |> Map.ofArray


let elementsInName (elements : Map<string, Element>) (name : string) =
    let rec keepProcessing idx checkTwo matches =
        match name.Length - idx with
        | x when x = 0 -> Some(matches)
        | x when x > 1 && checkTwo -> 
            let key = name.Substring(idx, 2)
            match elements.ContainsKey(key) with
            | false -> keepProcessing idx false matches
            | true -> 
                let solution = keepProcessing (idx + 2) true (elements.[key] :: matches)
                match solution with
                | None -> keepProcessing (idx + 2) false matches
                | Some(y) -> Some(y)
        | _ -> 
            let key = name.Substring(idx, 1)
            match elements.ContainsKey(key) with
            | false -> None
            | true -> keepProcessing (idx + 1) true (elements.[name.Substring(idx, 1)] :: matches)

    keepProcessing 0 true []


let allElements = getElements @"C:\Projects\RedditProgrammingChallanges\Reddit2017\elements.txt"
printfn "Number of elements: %i" allElements.Length
let mapped = mapElements allElements

let solution = elementsInName (mapElements allElements) ("genius".ToUpperInvariant())
match solution with
| None -> printfn "No Solution"
| Some(y) -> printfn "%s (%s)" (y |> List.fold(fun s e -> e.Symbol + s) "") (y |> List.fold(fun s e -> e.Name + " " + s) "")