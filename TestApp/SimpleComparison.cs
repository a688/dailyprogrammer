﻿using System;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Attributes.Columns;

namespace TestApp
{
    [MinColumn, MaxColumn]
    public class SimpleComparison
    {
        public const int ASCII_ZERO = 48;
        public const int ASCII_NINE = 57;
        public const int ASCII_MINUS = 45;

        private static string v;

        public SimpleComparison()
        {
            v = "N";
        }

        //[Benchmark]
        //public static bool caseStatement()
        //{
        //    switch (v)
        //    {
        //        case "Y": return true;
        //        case "N": return false;
        //        default: throw new Exception("WUT!");
        //    }
        //}

        //[Benchmark]
        //public static bool ifStatement()
        //{
        //    if (string.Equals(v, "Y", StringComparison.CurrentCulture)) { return true; }
        //    if (string.Equals(v, "N", StringComparison.CurrentCulture)) { return false; }
        //    throw new Exception("WUT!");
        //}

        [Benchmark]
        public static void Int32Cast_GoodInt()
        {
            Int3Cast("123456");
        }

        [Benchmark]
        public static void Int32Check_GoodInt()
        {
            IntCheck("123456");
        }

        [Benchmark]
        public static void Int32Cast_BadInt()
        {
            Int3Cast("abdedfg");
        }

        [Benchmark]
        public static void Int32Check_BadInt()
        {
            IntCheck("abdedfg");
        }

        [Benchmark]
        public static void Int32Cast_Negative()
        {
            Int3Cast("-134");
        }

        [Benchmark]
        public static void Int32Check_Negative()
        {
            IntCheck("-134");
        }



        public static void Int3Cast(string i)
        {
            int goodInt;
            if (!Int32.TryParse(i, out goodInt)) { return; }
            
            return;
        }

        public static void IntCheck(string i)
        {
            var asciiValOfFirstChar = System.Convert.ToInt32(i[0]);
            if ((asciiValOfFirstChar < ASCII_ZERO) || (asciiValOfFirstChar > ASCII_NINE))
            {
                if (asciiValOfFirstChar != ASCII_MINUS)
                {
                    return;
                }
            }
            return;
        }
    }
}
