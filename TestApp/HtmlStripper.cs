﻿using System;
using System.Text.RegularExpressions;

namespace TestApp
{
    public static class HtmlStripper
    {
        public static void StripText()
        {
            try
            {
                var initialFile = System.IO.File.ReadAllText(@"C:\temp\firstempire.txt.htm");

                var innerPattern = "\\s?[a-zA-Z|0-9|=|\\-|_|:|&|,|;|\"|'|\\.|\\s|\\r|\\n|\\(|\\)]*>";

                var withoutJunk = Regex.Replace(initialFile.Replace("\r\n", ""), "</?(span|a|img|tbody|hr)" + innerPattern, "");
                var withoutStartTags = Regex.Replace(withoutJunk, "<(div|table|tr|td)" + innerPattern, "");

                var withNewLines = Regex.Replace(withoutStartTags, "</?\\s?(div|br|table|tr)" + innerPattern, "\r\n");
                var tabbedTags = Regex.Replace(withNewLines, "</td" + innerPattern, "\t");

                System.IO.File.WriteAllText(@"C:\Temp\converted.txt", UrlDecode(tabbedTags));
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private static string UrlDecode(string original)
        {
            return original.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&nbsp;", " ");
        }
    }
}
