﻿using System;
using BenchmarkDotNet.Running;

namespace TestApp
{
    class Program
    {

        static void Main(string[] args)
        {
            var summary = BenchmarkRunner.Run<SimpleComparison>();

            System.Console.ReadLine();
        }
    }
}
