﻿type TraceBuilder() =
    member this.Bind(m, f) = 
        match m with 
        | None -> 
            printfn "Binding with None. Exiting."
        | Some a -> 
            printfn "Binding with Some(%A). Continuing" a
        Option.bind f m

    member this.Return(x) = 
        printfn "Return an unwrapped %A as an option" x
        Some x

    member this.Zero() = 
        printfn "Zero"
        None

    member this.Combine (a,b) = 
        printfn "Returning early with %A. Ignoring second part: %A" a b 
        a

    member this.Delay(funcToDelay) = 
        let delayed = fun () ->
            printfn "%A - Starting Delayed Fn." funcToDelay
            let delayedResult = funcToDelay()
            printfn "%A - Finished Delayed Fn. Result is %A" funcToDelay delayedResult
            delayedResult  // return the result 

        printfn "%A - Delaying using %A" funcToDelay delayed
        delayed // return the new function

    member this.Run(funcToRun) = 
        printfn "%A - Run Start." funcToRun
        let runResult = funcToRun()
        printfn "%A - Run End. Result is %A" funcToRun runResult
        runResult // return the result of running the delayed function

// make an instance of the workflow                
let trace = new TraceBuilder()

trace { 
    printfn "Part 1: about to return 1"
    return 1
    printfn "Part 2: after return has happened"
    } 

// -- OUTPUT ------------------------
// <fun:it@42-2> - Delaying using <fun:delayed@23>                      (delaying the overall expression (the output of Combine))
// <fun:delayed@23> - Run Start.                                        (running the outermost delayed expression (the output of Combine))
// <fun:it@42-2> - Starting Delayed Fn.
// Part 1: about to return 1
// Return an unwrapped 1 as an option                                   (the first expression results in Some(1))
// <fun:it@44-3> - Delaying using <fun:delayed@23>                      (the second expression is wrapped in a delay)
// Returning early with Some 1. Ignoring second part: <fun:delayed@23>  (the first and second expressions are combined)
// <fun:it@42-2> - Finished Delayed Fn. Result is Some 1                (overall delayed expression (the output of Combine) is complete)
// <fun:delayed@23> - Run End. Result is Some 1
// val it : int option = Some 1                                         (the result is now an Option not a function)