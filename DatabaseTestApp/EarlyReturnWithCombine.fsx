﻿type TraceBuilder() =
    member this.Bind(m, f) = 
        match m with 
        | None -> 
            printfn "Binding with None. Exiting."
        | Some a -> 
            printfn "Binding with Some(%A). Continuing" a
        Option.bind f m

    member this.Return(x) = 
        printfn "Return an unwrapped %A as an option" x
        Some x

    member this.Zero() = 
        printfn "Zero"
        None

    member this.Combine (m,f) = 
        printfn "Combine. Starting second param %A" f
        let y = f()
        printfn "Combine. Finished second param %A. Result is %A" f y

        match m,y with
        | Some a, Some b ->
            printfn "combining %A and %A" a b 
            Some (a + b)
        | Some a, None ->
            printfn "combining %A with None" a 
            Some a
        | None, Some b ->
            printfn "combining None with %A" b 
            Some b
        | None, None ->
            printfn "combining None with None"
            None

    member this.Delay(funcToDelay) = 
        let delayed = fun () ->
            printfn "%A - Starting Delayed Fn." funcToDelay
            let delayedResult = funcToDelay()
            printfn "%A - Finished Delayed Fn. Result is %A" funcToDelay delayedResult
            delayedResult  // return the result 

        printfn "%A - Delaying using %A" funcToDelay delayed
        delayed // return the new function

    member this.Run(funcToRun) = 
        printfn "%A - Run Start." funcToRun
        let runResult = funcToRun()
        printfn "%A - Run End. Result is %A" funcToRun runResult
        runResult // return the result of running the delayed function

// make an instance of the workflow                
let trace = new TraceBuilder()

trace { 
    return 1
    return 2
    } |> printfn "Result for return then return: %A"


// -- OUTPUT -----------------------------
// - entire expression is delayed
// <fun:clo@318-69> - Delaying using <fun:delayed@295-6>
// 
// - entire expression is run
// <fun:delayed@295-6> - Run Start.
// 
// - delayed entire expression is run
// <fun:clo@318-69> - Starting Delayed Fn.
// 
// - first return
// Returning a unwrapped 1 as an option
// 
// - delaying second return
// <fun:clo@319-70> - Delaying using <fun:delayed@295-6>
// 
// combine starts
// Combine. Starting second param <fun:delayed@295-6>
// 
//     // delayed second return is run inside Combine
//     <fun:clo@319-70> - Starting Delayed Fn.
//     Returning a unwrapped 2 as an option
//     <fun:clo@319-70> - Finished Delayed Fn. Result is Some 2
//     // delayed second return is complete
// 
// - Combine. Finished second param <fun:delayed@295-6>. Result is Some 2
// combining 1 and 2
// - combine is complete
// 
// <fun:clo@318-69> - Finished Delayed Fn. Result is Some 3
// - delayed entire expression is complete
// 
// <fun:delayed@295-6> - Run End. Result is Some 3
// - Run is complete
// 
// - final result is printed
// Result for return then return: Some 3