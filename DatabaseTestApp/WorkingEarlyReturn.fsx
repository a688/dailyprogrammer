﻿type TraceBuilder() =
    member this.Bind(m, f) = 
        match m with 
        | None -> 
            printfn "Binding with None. Exiting."
        | Some a -> 
            printfn "Binding with Some(%A). Continuing" a
        Option.bind f m

    member this.Return(x) = 
        printfn "Return an unwrapped %A as an option" x
        Some x

    member this.Zero() = 
        printfn "Zero"
        None

    member this.Combine (a,b) = 
        printfn "Returning early with %A. Ignoring second part: %A" a b 
        a

    member this.Delay(funcToDelay) = 
        printfn "DELAY"
        let delayed = fun () ->
            printfn "%A - Starting Delayed Fn." funcToDelay
            let delayedResult = funcToDelay()
            printfn "%A - Finished Delayed Fn. Result is %A" funcToDelay delayedResult
            delayedResult  // return the result 

        printfn "%A - Delaying using %A" funcToDelay delayed
        delayed // return the new function

// make an instance of the workflow                
let trace = new TraceBuilder()

let f = trace { 
    printfn "Part 1: about to return 1"
    return 1
    printfn "Part 2: after return has happened"
    } 
// -- OUTPUT ------------------------
// <fun:f@36> - Delaying using <fun:delayed@23>
// val f : (unit -> int option)

// f is assigned the result of this.Delay's delayed function (which is the first function in the sequence)


f() |> printfn "Result for Part1 without Part2: %A"  
// -- OUTPUT ------------------------
// <fun:f@36> - Starting Delayed Fn.
// Part 1: about to return 1
// Return an unwrapped 1 as an option
// <fun:f@38-1> - Delaying using <fun:delayed@23>
// Returning early with Some 1. Ignoring second part: <fun:delayed@23>
// <fun:f@36> - Finished Delayed Fn. Result is Some 1
// Result for Part1 without Part2: Some 1
// val it : unit = ()

// If we assigned the output of f() to a variable we would see that is is the delayed 'printfn "Part 2:"' part of the sequence