﻿let combine a b = 
    match a with
    | Some _ -> a  // a succeeds -- use it
    | None -> b    // a fails -- use b instead

// create an infix version
let ( <++ ) = combine

let map1 = [ ("1","One"); ("2","Two") ] |> Map.ofList
let map2 = [ ("A","Alice"); ("B","Bob") ] |> Map.ofList

let result = 
    (map1.TryFind "A") 
    <++ (map1.TryFind "B")
    <++ (map2.TryFind "A")
    <++ (map2.TryFind "B")
    |> printfn "Result of adding options is: %A"