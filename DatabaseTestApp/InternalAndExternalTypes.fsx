﻿// The output of Return is passed into Delay, so they must have compatible types.
// The output of Delay is passed into the second parameter of Combine.
// The output of Delay is also passed into Run.
//  The output of return doesn't have to be public

type Internal = Internal of int option
type Delayed = Delayed of (unit -> Internal)

type TraceBuilder() =
    member this.Bind(m, f) = 
        match m with 
        | None -> 
            printfn "Binding with None. Exiting."
        | Some a -> 
            printfn "Binding with Some(%A). Continuing" a
        Option.bind f m

    member this.Return(x) = 
        printfn "Returning a unwrapped %A as an option" x
        Internal (Some x) 

    member this.ReturnFrom(m) = 
        printfn "Returning an option (%A) directly" m
        Internal m

    member this.Zero() = 
        printfn "Zero"
        Internal None

    member this.Combine (Internal x, Delayed g) : Internal = 
        printfn "Combine. Starting %A" g
        let (Internal y) = g()
        printfn "Combine. Finished %A. Result is %A" g y
        let o = 
            match x,y with
            | Some a, Some b ->
                printfn "Combining %A and %A" a b 
                Some (a + b)
            | Some a, None ->
                printfn "combining %A with None" a 
                Some a
            | None, Some b ->
                printfn "combining None with %A" b 
                Some b
            | None, None ->
                printfn "combining None with None"
                None
        // return the new value wrapped in a Internal
        Internal o                

    member this.Delay(funcToDelay) = 
        let delayed = fun () ->
            printfn "%A - Starting Delayed Fn." funcToDelay
            let delayedResult = funcToDelay()
            printfn "%A - Finished Delayed Fn. Result is %A" funcToDelay delayedResult
            delayedResult  // return the result 

        printfn "%A - Delaying using %A" funcToDelay delayed
        Delayed delayed // return the new function wrapped in a Delay

    member this.Run(Delayed funcToRun) = 
        printfn "%A - Run Start." funcToRun
        let (Internal runResult) = funcToRun()
        printfn "%A - Run End. Result is %A" funcToRun runResult
        runResult // return the result of running the delayed function

// make an instance of the workflow                
let trace = new TraceBuilder()

trace { 
    return 1
    return 2
    return 3
    } |> printfn "Result for return x 3: %A" 


// -- OUTPUT -----------------------
// <fun:it@71> - Delaying using <fun:delayed@48>
// <fun:delayed@48> - Run Start.
// <fun:it@71> - Starting Delayed Fn.
// Returning a unwrapped 1 as an option
// <fun:it@72-1> - Delaying using <fun:delayed@48>
// Combine. Starting <fun:delayed@48>
// <fun:it@72-1> - Starting Delayed Fn.
// Returning a unwrapped 2 as an option
// <fun:it@73-2> - Delaying using <fun:delayed@48>
// Combine. Starting <fun:delayed@48>
// <fun:it@73-2> - Starting Delayed Fn.
// Returning a unwrapped 3 as an option
// <fun:it@73-2> - Finished Delayed Fn. Result is Internal (Some 3)
// Combine. Finished <fun:delayed@48>. Result is Some 3
// Combining 2 and 3
// <fun:it@72-1> - Finished Delayed Fn. Result is Internal (Some 5)
// Combine. Finished <fun:delayed@48>. Result is Some 5
// Combining 1 and 5
// <fun:it@71> - Finished Delayed Fn. Result is Internal (Some 6)
// <fun:delayed@48> - Run End. Result is Some 6
// Result for return x 3: Some 6
// val it : unit = ()