﻿// As with all the builder methods, if you don't need them, you don't need to implement them. So for a workflow that is 
// strongly sequential, you could easily create a builder class with Combine, Zero, and Yield, say, without having to 
// implement Bind and Return at all.

type TraceBuilder() =

    member this.ReturnFrom(x) = 
        printfn "ReturnFrom"
        x

    member this.Zero() = 
        printfn "Zero"
        Some ()            // Equivilent to Some(<unit>)

    member this.Combine (a,b) = 
        printfn "Combine %A and %A" a b
        a |> Option.bind (fun ()-> b )

    member this.Delay(f) = 
        printfn "Delay"
        f()

// make an instance of the workflow
let trace = new TraceBuilder()


trace { 
    if true then printfn "hello......."
    if false then printfn ".......world"
    return! Some 1
    } |> printfn "Result for minimal combine: %A" 

// --- Output is  ----------------------------------------------------
// Delay                                    (Called before anything happens)
// hello.......                             (True part of the if statement)
// Zero                                     (value of the if statement)
// Delay                                    (Called before the second if statement)
// Zero                                     (value of the if statement)
// Delay                                    (Called before the return! statement)
// ReturnFrom                               (return!)
// Combine                                  (Combining second if's <unit> with return!'s 1)
// Combine                                  (Combining first if's <unit> with the result of the prior combine)
// Result for minimal combine: Some 1       (final result of the expression)
// val it : unit = ()