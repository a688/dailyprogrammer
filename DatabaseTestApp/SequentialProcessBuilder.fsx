﻿// If the workflow has the concept of sequential steps, then the overall result is just the value of the last step, and 
// all the previous steps are evaluated only for their side effects.

// The equivalent approach for a computation expression is to treat each expression (other than the last) as a wrapped unit 
// value, and "pass it into" the next expression, and so on, until you reach the last expression.

// This is exactly what bind does, of course, and so the easiest implementation is just to reuse the Bind method itself. 
// Also, for this approach to work it is important that Zero is the wrapped unit value.

type TraceBuilder() =
    member this.Bind(m, f) = 
        match m with 
        | None -> 
            printfn "Binding with None. Exiting."
        | Some a -> 
            printfn "Binding with Some(%A). Continuing" a
        Option.bind f m

    member this.Return(x) = 
        printfn "Returning a unwrapped %A as an option" x
        Some x

    member this.Yield(x) = 
        printfn "Yield an unwrapped %A as an option" x
        Some x

    member this.ReturnFrom(m) = 
        printfn "Returning an option (%A) directly" m
        m

    member this.YieldFrom(m) = 
        printfn "Yield an option (%A) directly" m
        m

    member this.Zero() = 
        printfn "Zero"
        this.Return ()  // unit not None

    // The difference from a normal bind is that the continuation has a unit parameter, and evaluates to b. This in turn 
    // forces a to be of type WrapperType<unit> in general, or unit option in our case.
    member this.Combine (a,b) = 
        printfn "Combining %A with %A" a b
        this.Bind( a, fun ()-> b )

    member this.Delay(f) = 
        printfn "Delay"
        f()


// make an instance of the workflow 
let trace = new TraceBuilder()

trace { 
    if true then printfn "hello......."
    if false then printfn ".......world"
    return 1
    } |> printfn "Result for sequential combine: %A" 

/// ---- outputs-----------------------------------------
/// Delay (called at very beginning)
/// hello....... (the 'then' of the if statement)
/// Zero (the 'else' of the if statement)
/// Returning a unwrapped <null> as an option (yielding the if statement)
/// Delay (called before the second line executes)
/// Zero (the 'else' of the if statement)
/// Returning a unwrapped <null> as an option (yielding the if statement)
/// Delay (called before the third line executes)
/// Returning a unwrapped 1 as an option (returning the 1 as per the line)
/// Combining Some null with Some 1 (Combin[e]ing the return value of the second if statemnet (Zero) with the 1 from the return statement)
/// Binding with Some(<null>). Continuing (bind called using <null> (unit) and <() => 1> as a and b)
/// Combining Some null with Some 1 (Combin[e]ing the return value of the first if statemnet (Zero) with result of the prior combine Some(1))
/// Binding with Some(<null>). Continuing (bind called using <null> (unit) and <() => 1> as a and b)
/// Result for sequential combine: Some 1 (final output is the 1 from the return)