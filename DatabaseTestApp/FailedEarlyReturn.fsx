﻿type TraceBuilder() =
    member this.Bind(m, f) = 
        match m with 
        | None -> 
            printfn "Binding with None. Exiting."
        | Some a -> 
            printfn "Binding with Some(%A). Continuing" a
        Option.bind f m

    member this.Return(x) = 
        printfn "Return an unwrapped %A as an option" x
        Some x

    member this.Zero() = 
        printfn "Zero"
        None

    member this.Combine (a,b) = 
        printfn "Returning early with %A. Ignoring second part: %A" a b 
        a

    member this.Delay(f) = 
        printfn "Delay"
        f()

// make an instance of the workflow                
let trace = new TraceBuilder()

trace { 
    printfn "Part 1: about to return 1"
    return 1
    printfn "Part 2: after return has happened"
    } |> printfn "Result for Part1 without Part2: %A"  

// ---- Output ---------------------------------
// Delay                                                (called before anything else)
// Part 1: about to return 1                            (print line)
// Return an unwrapped 1 as an option                   (return statement)
// Delay                                                (called before the final brace)
// Part 2: after return has happened                    (print line)
// Zero                                                 (Zero here because no explicit return was given for this part)
// Returning early with Some 1. Ignoring second part: <null>     (combining the two expressions)
// Result for Part1 without Part2: Some 1               (final result)

// the "part 2" line executes because a computational expressions ALWAYS runs to the last curley brace