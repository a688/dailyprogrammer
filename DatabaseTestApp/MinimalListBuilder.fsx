﻿type ListBuilder() =

    member this.Yield(x) = [x]

    member this.For(m,f) =
        m |> List.collect f

    member this.Combine (a,b) = 
        List.concat [a;b]

    member this.Delay(f) = f()

// make an instance of the workflow                
let listbuilder = new ListBuilder()

listbuilder { 
    yield 1
    yield 2
    } |> printfn "Result: %A" 

let z = 5

listbuilder { 
    for i in [1..z] do yield i + 2
    yield 42
    } |> printfn "Result: %A" 