﻿let msqFormat = new System.Messaging.ActiveXMessageFormatter()

let rec getMessages (path : string) f = 
        use msgQ = new System.Messaging.MessageQueue(path)
        msgQ.Formatter <- msqFormat
        let getMessage = msgQ.BeginReceive()
        let msg = msgQ.EndReceive(getMessage)
        f msg
        msgQ.Close()
        getMessages path f  // Tail recursion

let rec SendMessage (path : string) =
    use msgQ = new System.Messaging.MessageQueue(path)
    use msg = new System.Messaging.Message(System.DateTime.Now.ToString("yyyyMMdd HHmmss"), msqFormat)
    msgQ.Send(msg)
    msgQ.Close()
    System.Threading.Thread.Sleep(2000)
    SendMessage path

let doTest (argv : array<string>) = 
    let queueName = "FormatName:DIRECT=OS:wmemjlonberger\\private$\\TestQueue"
    printfn "%s" queueName
    printfn "%i" argv.Length

    let cts = new System.Threading.CancellationTokenSource()
    
    if (argv.Length = 0) then
        SendMessage queueName
    else
        let reader = async { getMessages queueName (fun (x) -> printfn "%s" (x.Body.ToString())) } 
        Async.StartAsTask(reader, cancellationToken = cts.Token) |> ignore

    System.Console.WriteLine("Press enter to exit")
    System.Console.ReadLine() |> ignore
    cts.Cancel()
