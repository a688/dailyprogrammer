﻿let toByteBase value =
    let rec convert remaining acc =
        match remaining with
        | x when x < 255 -> (acc + " " + remaining.ToString()).Trim()
        | _ -> convert (remaining - 255) (acc + " 255")

    convert value ""

let convertToByteBase (value : string) = 
    value.Split([| ' ' |]) |> Array.map(System.Convert.ToInt32) |> Array.map(toByteBase)

let convertFromByteBase (value : string) =
    let toParse = value.Split([| ' ' |]) |> Array.map(System.Convert.ToInt32) |> List.ofSeq

    let rec build remaining currentSum (allInts : list<int>) =
        match remaining with 
        | [] -> allInts
        | head::tail -> 
            match head with 
            | x when x = 255 -> build tail (currentSum + 255) allInts
            | _ -> build tail 0 ((currentSum + head) :: allInts)

    (build toParse 0 []) |> List.rev


toByteBase 510
convertFromByteBase "255 255 2 44 255 255 255 255 4"
