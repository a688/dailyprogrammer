﻿open System

let makeLarger (solid : int) (liquid : int) (currentMultiplier : int) =
    let rec innerModifier liq multiplier =
        match solid > liq with
        | false -> liq, multiplier
        | true -> innerModifier (liq + 10) (multiplier + 1)
    innerModifier (liquid + (10 * currentMultiplier)) currentMultiplier

let rec adjustNumbers (previousNumber : int) (multiplier : int) (remaining : list<int>) (output : list<int>) = 
    match remaining with 
    | [] -> output |> List.rev
    | head::tail ->
        match head > previousNumber with
        | true -> 
            adjustNumbers head multiplier tail (head :: output)
        | false -> 
            let newNumber, newPower = makeLarger previousNumber head multiplier
            adjustNumbers newNumber newPower tail (newNumber :: output)

let buildRange (currentNumber : int) (lastNumberMatch : string) (step : int) =
    let rec innerBuilder (nbr : int) (output : list<int>) = 
        match nbr.ToString().EndsWith(lastNumberMatch) with
        | true -> (nbr :: output) |> List.rev |> Array.ofList
        | false -> innerBuilder (nbr + step) (nbr :: output)
    innerBuilder currentNumber []
    
let extractSection (section : string) (seperator : string) =
    let parts = section.Split([| seperator |], System.StringSplitOptions.RemoveEmptyEntries)
    let step = 
        match parts.Length with 
        | x when x = 3 -> (int)parts.[2]
        | _ -> 1
    buildRange ((int)parts.[0]) parts.[1] step

let processLine (line : string) = 
    let toAdjust = 
        line.Split([| "," |], System.StringSplitOptions.RemoveEmptyEntries)
        |> Array.map(
            fun x -> 
                match x.Contains(":"), x.Contains("."), x.Contains("-") with
                | true, _, _ -> extractSection x ":"
                | _, true, _ -> extractSection x "." 
                | _, _, true -> extractSection x "-"
                | _ -> [| System.Int32.Parse(x) |]
            )
        |> Array.collect(id)
        |> List.ofArray

    adjustNumbers 0 0 toAdjust []



//printfn "%A" (processLine "1,3,7,2,4,1")
//printfn "%A" (processLine "1-3,1-2")
//printfn "%A" (processLine "1:5:2")
//printfn "%A" (processLine "104-2")
printfn "%A" (processLine "104..02")