﻿let rand = new System.Random();

let swap (a: _[]) x y =
    let tmp = a.[x]
    a.[x] <- a.[y]
    a.[y] <- tmp

// shuffle an array (in-place)
let shuffle a =
    Array.iteri (fun i _ -> swap a i (rand.Next(i, Array.length a))) a

let headToHeadWinner p1 p2 (skillz : array<double>) = 
    let prob = rand.NextDouble() * (skillz.[p1] + skillz.[p2])
    match prob <= skillz.[p1] with
    | true -> p1
    | false -> p2

let rec findWinner (players : array<int>) (skillz : array<double>) =
    match players.Length with
    | 1 -> players.[0]
    | _ -> 
        shuffle players
        let combinations = players |> Array.chunkBySize 2
        let winners = combinations |> Array.map(fun il -> headToHeadWinner il.[0] il.[1] skillz)
        findWinner winners skillz

//let skillz = [| 1.0; 2.0; 3.0; 4.0; 5.0; 6.0; 7.0; 8.0; 9.0; 10.0; 11.0; 12.0; 13.0; 14.0; 15.0; 16.0 |]
let skillz = [| 5.0; 10.0; 13.0; 88.0; 45.0; 21.0; 79.0; 9.0; 56.0; 21.0; 90.0; 55.0; 17.0; 35.0; 85.0; 34.0 |]
let players = Array.init (skillz.Length) (fun x -> x)
let mutable wins = Array.zeroCreate (skillz.Length)
let mutable runCount = 0

let timer = System.Diagnostics.Stopwatch.StartNew()

while runCount < 10000000 do
    let winner = findWinner players skillz
    wins.[winner] <- wins.[winner] + 1
    runCount <- runCount + 1

timer.Stop()

let percentages = 
    wins |> Array.map(fun x -> System.Convert.ToDouble(x) / System.Convert.ToDouble(runCount))

printfn "Percentages %A" percentages
printfn "Took %f seconds" (timer.Elapsed.TotalSeconds)