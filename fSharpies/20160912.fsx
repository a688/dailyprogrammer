﻿let lettersOnly (w : string) = w.Replace(" ", "").ToLower() |> Seq.sort |> Seq.reduce(+)
let wordArray (w : string) = w.ToLower().Split([|' '|]) |> Array.sort
let isJustRearranged (w1 : array<string>) (w2 : array<string>) =
    match w1.Length = w2.Length with
    | false -> false
    | true ->
        Array.zip w1 w2
        |> Array.filter(fun (a, b) -> System.String.Equals(a, b)) 
        |> Array.length = (w1.Length)
let isAnagram  (w1 : string) (w2 : string) = 
    match System.String.Equals(lettersOnly w1, lettersOnly w2), (isJustRearranged (wordArray w1) (wordArray w2)) with
    | true, false -> ""
    | _ -> " NOT"

let checkIt (words : array<string>) = printfn "\"%s\" is%s an anagram of \"%s\"" words.[0] (isAnagram words.[0] words.[1]) words.[1]
let processLine (line : string) = line.Replace("\"", "").Split([| " ? "|], System.StringSplitOptions.RemoveEmptyEntries)

checkIt (processLine "wisdom ? mid sow")
checkIt (processLine "Seth Rogan ? Gathers No")
checkIt (processLine "Reddit ? Eat Dirt")
checkIt (processLine "Schoolmaster ? The classroom")
checkIt (processLine "Astronomers ? Moon starer")
checkIt (processLine "Vacation Times ? I'm Not as Active")
checkIt (processLine "Dormitory ? Dirty Rooms")
checkIt (processLine "This shall not pass ? Pass this shall not")
checkIt (processLine "This shall not pass ? Past shis shall not")