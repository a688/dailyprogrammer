﻿// The Rules for the game are in the link below:
// http://www.wikihow.com/Count-to-99-on-Your-Fingers
// Effectively, you count "outwards" starting with your index finger and your
// thumb acts as a place holder for either 50 (on your left hand) or 5 (on your right)

// Fingers is an integer that represents your fingers (left to right, left pinky to right pinky)

let handValue fingers multiplier = 
    let total = 4 - ([| 0xF; 0xE; 0xC; 0x8; 0x0 |] |> Array.findIndex(fun x -> (x &&& fingers) = x))
    let thumb = ((fingers >>> 4) &&& 0x1) * 5

    if (total = 0) && ((fingers &&& 0xF) > 0) then -100
    else (total + thumb) * multiplier

let flipHand hand = 
    seq { for x in 0..4 do yield ((hand >>> (5 + x)) &&& 0x1) <<< (4 - x) } |> Seq.reduce(|||)

let total allFingers =
    let sum = (handValue (flipHand allFingers) 10) + (handValue allFingers 1)
    if sum < 0 then "Invalid"
    else sum.ToString()

printfn "%s" (total ((1 <<< 8) ||| (1 <<< 7) ||| (1 <<< 6) ||| (1 <<< 4) ||| (1 <<< 3) ||| (1 <<< 2)));;
printfn "%s" (total ((1 <<< 8)));;
printfn "%s" (total ((1 <<< 7) ||| (1 <<< 6) ||| (1 <<< 5) ||| (1 <<< 3) ||| (1 <<< 2) ||| (1 <<< 1)));;
printfn "%s" (total ((1 <<< 5) ||| (1 <<< 4)));;
printfn "%s" (total ((1 <<< 9) ||| (1 <<< 8) ||| (1 <<< 7) ||| (1 <<< 6) ||| (1 <<< 5) ||| (1 <<< 4) ||| (1 <<< 0)));;

// 9876543210
// 0111011100 -> 37
// 1010010000 -> Invalid#
// 0011101110 -> 73
// 0000110000 -> 55
// 1111110001 -> Invalid