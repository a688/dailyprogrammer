﻿let fibs = Seq.unfold(fun (a,b) -> Some (b, (b, a + b))) (0I,1I) |> Seq.cache

let zeckendorfsTheorem (toFind : bigint) =
    let possibleFibs = fibs |> Seq.takeWhile(fun x -> x <= toFind) |> Seq.rev |> Array.ofSeq

    let rec findAnswer remainder fibIndex acc =
        match remainder, fibIndex, possibleFibs.Length with
        | r, _, _ when r = 0I -> Some(acc |> List.reduceBack(fun i acc -> acc + " + " + i))
        | _, x, y when x >= y -> None
        | r, i, _ -> 
            let isSolution = findAnswer (remainder - possibleFibs.[fibIndex]) (fibIndex + 2) (possibleFibs.[fibIndex].ToString() :: acc)
            match isSolution with
            | Some _ -> isSolution
            | None -> findAnswer remainder (fibIndex + 1) acc

    findAnswer toFind 0 [ ]


zeckendorfsTheorem 5I       // 5
zeckendorfsTheorem 120I     // 89 + 21 + 8 + 2
zeckendorfsTheorem 34I      // 34
zeckendorfsTheorem 88I      // 55 + 21 + 8 + 3 + 1
zeckendorfsTheorem 90I      // 89 + 1
zeckendorfsTheorem 320I     // 233 + 55 + 21 + 8 + 3