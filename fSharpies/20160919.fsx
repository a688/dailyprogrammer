﻿open System.Collections.Generic

let firstAndLast (word : string) = 
    word.Substring(0, 1) + word.Substring(word.Length - 1, 1)

let compileDictionary (allWords : array<string>) = 
    let dict = new Dictionary<string, List<string>>()

    allWords 
    |> Array.filter(fun w -> w.Length > 4)
    |> Array.iter(fun w -> 
        let fl = firstAndLast w
        if (dict.ContainsKey(fl) = false) then
            dict.Add(fl, new List<string>())
        dict.[fl].Add(w)
    )

    dict

let isSimilar (swipedWord : string) (possibleResponse : string) =
    let rec canMatch (prRemaining : list<char>) (swRemaining : list<char>) previousChar =
        match prRemaining, swRemaining with
        | [], _ -> Some()
        | _, [] -> None
        | head1::tail1, head2::tail2 -> 
            match (head1 = head2), (previousChar = head1) with
            | false, true -> canMatch tail1 tail2 ' '
            | true, _ -> canMatch tail1 tail2 head2
            | _ -> canMatch prRemaining tail2 previousChar

    (canMatch (possibleResponse |> Seq.toList) (swipedWord |> Seq.toList) ' ').IsSome

let matches (w : string) (dict : Dictionary<string, List<string>>) =
    let fl = firstAndLast w

    let results = 
        match dict.ContainsKey(fl) with
        | false -> ""
        | true -> 
            let allMatches = dict.[fl] |> Seq.filter(fun x -> isSimilar w x)
            if (allMatches |> Seq.length) = 0 then ""
            else allMatches |> Seq.reduce(fun acc t -> acc + "," + t)

    if results.Length = 0 then sprintf "No matches possible for %s" w
    else sprintf "Matches for %s: %s" w results

let file = System.IO.File.ReadAllLines(__SOURCE_DIRECTORY__ + @"\20160919.txt")
let myDict = compileDictionary file

printfn "Distinct groupings %i" myDict.Count

printfn "%s" (matches "qwertyuytresdftyuioknn" myDict)
printfn "%s" (matches "gijakjthoijerjidsdfnokg" myDict)