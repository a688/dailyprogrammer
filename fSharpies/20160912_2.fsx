﻿let breakDownWord (w : string) = w.ToLower().Split([|' '|], System.StringSplitOptions.RemoveEmptyEntries) |> Seq.sort
let combineSeq w = w |> Seq.collect(id) |> Seq.filter(System.Char.IsLetter) |> Seq.sort
let anagramResult (a : string) (b : string) = 
    let (x, y) = ((breakDownWord a), (breakDownWord b))
    match ((x, y) ||> Seq.forall2 (=)) with
    | true -> false 
    | false -> (((combineSeq x), (combineSeq y)) ||> Seq.forall2 (=))

let IsOrNot isMatch = if isMatch then " " else " NOT"
let checkIt (words : array<string>) = printfn "\"%s\" is%s an anagram of \"%s\"" words.[0] (IsOrNot (anagramResult words.[0] words.[1])) words.[1]
let processLine (line : string) = line.Replace("\"", "").Split([| " ? "|], System.StringSplitOptions.RemoveEmptyEntries)

printfn "%b" (anagramResult "wisdom" "mid sow")
printfn "%b" (anagramResult "Seth Rogan" "Gathers No")
printfn "%b" (anagramResult "Reddit" "Eat Dirt")
printfn "%b" (anagramResult "Schoolmaster" "The classroom")
printfn "%b" (anagramResult "Astronomers" "Moon starer")
printfn "%b" (anagramResult "Vacation Times" "I'm Not as Active")
printfn "%b" (anagramResult "Dormitory" "Dirty Rooms")
printfn "%b" (anagramResult "This shall not pass" "Pass this shall not")
printfn "%b" (anagramResult "This shall not pass" "Past shis shall not")








// From somebody else
let isAnagram (s1:string) (s2:string) =
    let wordSep = (fun (s:string) -> s.ToLower().Split([|' '|], System.StringSplitOptions.RemoveEmptyEntries) |> Seq.sort)
    let w1 = s1 |> wordSep
    let w2 = s2 |> wordSep
    match (w1,w2) ||> Seq.forall2 (=) with
    |true -> false
    |_ ->
        let collector = Seq.collect id >> Seq.filter System.Char.IsLetter >> Seq.sort
        let l1 = w1 |> collector
        let l2 = w2 |> collector
        (l1,l2) ||> Seq.forall2 (=) 