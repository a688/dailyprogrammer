﻿let minYear = 1900

let packDateParts (year : int) (month : int) (day : int) =
    (((year - minYear) &&& 0xFF) <<< 16) ||| ((month &&& 0xFF) <<< 8) ||| (day &&& 0xFF)

let packDate (date : System.DateTime) =
    packDateParts date.Year date.Month date.Day

let unpackDate value =
    new System.DateTime(((value >>> 16) &&& 0xFF) + minYear, (value >>> 8) &&& 0xFF, value &&& 0xFF)



unpackDate (packDate (new System.DateTime(2106, 01, 10)))
