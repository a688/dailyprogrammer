﻿let areSidesEqual (problem : string) = 
    let parts = problem.Split('=')

    let solveSide (side : string) = 
        side.Split([| "+" |], System.StringSplitOptions.RemoveEmptyEntries)
        |> Array.map(fun n -> System.Int32.Parse(n))
        |> Array.reduce(+)
    
    (solveSide parts.[0]) = (solveSide parts.[1])

let startingSet (repeatingCount : int) = Array.init 9 (fun x -> repeatingCount)

let quickReduceSet (originalSet : array<int>) (index : int) = 
    Array.init 9 (fun x -> if (x = index) then originalSet.[x] - 1 else originalSet.[x])

let removeExistingNumbers equation initialSet =
    let rec removeNumbers remaining finalSet =
        match remaining with 
        | [] -> finalSet
        | head::tail -> 
            match (int)head - 49 with
            | x when x >= 0 && x < 9 -> removeNumbers tail (quickReduceSet finalSet x)
            | _ -> removeNumbers tail finalSet
    removeNumbers (equation |> List.ofSeq) initialSet

let distinctNumbersRemaining set = set |> Array.mapi(fun i x -> if x = 0 then -1 else i + 1) |> Array.filter(fun x -> x > 0)

let nextIndex (set : array<int>) currentIndex =
    let rec picker idx =
        match idx with
        | x when x = -1 -> -1
        | _ -> 
            match set.[idx] with
            | y when y > 0 -> idx
            | _ -> picker (idx - 1)

    picker (currentIndex - 1)

let equation = "xxx + xxx + 5x3 + 123 = xxx + 795".Replace(" ", "")
let remainingSet = removeExistingNumbers equation (startingSet 2)

let rec findSolution (equation : list<string>) (remainingSet : array<int>) output =
    match equation with
    | [] -> 
        match areSidesEqual output with
        | true -> Some(output)
        | false -> None
    | head::tail ->
        match head with 
        | c when c = "x" -> 
            let distinctOptions = (distinctNumbersRemaining remainingSet) |> Array.sortDescending

            distinctOptions
            |> Array.tryPick(fun e -> 
                    let thisSet = quickReduceSet remainingSet (e - 1)
                    findSolution tail thisSet (output + (e.ToString()))
                )
        | _ -> findSolution tail remainingSet (output + head)

let finalSolution = findSolution (equation |> List.ofSeq |> List.map(fun c -> c.ToString())) remainingSet ""

printfn "%A" finalSolution