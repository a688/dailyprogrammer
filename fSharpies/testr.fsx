﻿// https://stackoverflow.com/questions/5028377/understanding-byref-ref-and
// https://stackoverflow.com/questions/3221200/f-let-mutable-vs-ref
//
// https://stackoverflow.com/questions/4577050/what-is-the-role-of-while-loops-in-computation-expressions-in-f
// https://stackoverflow.com/questions/38340711/f-exit-early-computation-expression?rq=1
// https://fsharpforfunandprofit.com/series/computation-expressions.html
// https://en.wikibooks.org/wiki/F_Sharp_Programming/Computation_Expressions

let divideBy denominator numerator =
    if (denominator = 0) then None
    else Some(numerator/denominator)


type MaybeBuilder() = 
    member this.Bind(x, rest) =
        match x with
        | None -> None
        | Some a -> rest a

    member this.Return(x) =
        Some x


type FibBuilder() =
    member this.Bind(x, rest) =
        match x with 
        | None -> None
        | Some a -> 
            match a with
            | 0 -> rest 1
            | _ -> rest a 

    member this.Yield(x) = 
        printfn "YIELD"
        Some x

    member this.Return(x) =
        x

    member this.While(guard, body) =
        if not (guard()) then None
        else this.Bind(body(), fun(x) -> this.While(guard, body))

    member this.Delay(f) =
        f

    member this.Zero(f) = 
        Some f

[<EntryPoint>]
let main argv = 
    let fibber = new FibBuilder()


    let mutable xer = 0
    let guard() = xer < 10
    let inc x = 
        printfn "%A" xer
        xer <- xer + x
        xer

    let fibulator(max) = 
        fibber {
            let incr = inc 1
            let fib = ref 0
            while guard() do
                fib := !fib + xer
                printfn "%A" fib
                yield xer
        }

    fibulator(99) |> ignore
    0