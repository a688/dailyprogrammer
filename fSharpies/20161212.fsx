﻿let swapLetters (wordA : string) (wordB : string) =
    let combined = (wordB |> Seq.zip wordA) |> Array.ofSeq

    let rec showResults startIndex allWords =
        match startIndex with 
        | x when x = wordA.Length -> allWords |> List.rev
        | _ -> 
            let output = 
                combined 
                |> Array.mapi(
                    fun i (l1, l2) -> 
                        match i with 
                        | z when z <= startIndex -> l2.ToString()
                        | _ -> l1.ToString()
                    )
            showResults (startIndex + 1) ((output |> Array.reduce(+)) :: allWords)

    printfn "StartIndex is %s" ((string)wordA)

    showResults 0 [ (string)wordA ]

swapLetters "wood" "like"

let swapLetters2 (wordA : string) (wordB : string) =
    let rec swap_em idx =
        match idx = wordA.Length with
        | true -> wordB
        | false -> 
            printfn "%s%s" (wordB.Substring(0, idx)) (wordA.Substring(idx, wordA.Length - idx))
            swap_em (idx + 1)
    printfn "%s" wordA
    printfn "%s" (swap_em 1)

swapLetters2 "wood" "like"
swapLetters2 "a fall to the floor" "braking the door in"