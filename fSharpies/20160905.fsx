﻿let fibs =   Seq.unfold(fun (a,b) -> Some (b, (b, a + b))) (0I,1I) |> Seq.cache

let rec mostOnes (orig : string) =
    let newS = orig.Replace("100", "011")
    if newS = orig then newS.TrimStart('0')
    else mostOnes newS

let minOnes (orig : string) = orig

let foldFnc = fun (n, acc) d -> if d <= n then (n-d, '1'::acc) else (n, '0'::acc)

let toFib (n : bigint) outputFunc =
    let fibbers = fibs |> Seq.takeWhile(fun x -> x <= n) |> Seq.rev |> Seq.toList
    let ans = fibbers |> Seq.fold foldFnc (n,[]) |> snd |> Seq.rev |> Seq.toArray |> System.String
    outputFunc ans

let toDec (n : seq<char>) = 
    Seq.zip (n |> Seq.rev) fibs |> Seq.choose(function ('1', b) -> Some b | _ -> None) |> Seq.sum

    
toDec "10"
toDec "1"
toDec "111111"
toDec "1010100101010100000010001000010010"

toFib 9024720I minOnes
toFib 9024720I mostOnes
toFib 8I minOnes
toFib 8I mostOnes