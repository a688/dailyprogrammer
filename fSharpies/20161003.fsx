﻿let findFactorial value =
    let negativeSign = if value < 0 then "-" else ""
    let rec fact value divisor =
        match value, value % divisor, value / divisor with 
        | _, x, _ when x <> 0 -> "NONE"
        | z, _, _ when z = 0 || z = 1 -> sprintf "%i!" z
        | _, _, y when y = (divisor + 1) -> sprintf "%i!" (divisor + 1)
        | _, _, y -> fact y (divisor + 1)
    printfn "%i = %s%s" value negativeSign (fact (System.Math.Abs(value)) 2)

let findFactorial2 value =
    let rec fact value divisor =
        match value, value % divisor with 
        | z, x when z < 0 || x <> 0 -> "NONE"
        | z, _ when z = 0 || z = 1 -> sprintf "%i!" z
        | _ -> 
            match value / divisor with 
            | y when y = 1 -> sprintf "%i!" value
            | y -> fact y (divisor + 1)

    printfn "%i = %s" value (fact value 2)

findFactorial 120
findFactorial 150
findFactorial 3628800
findFactorial 479001600
findFactorial 6
findFactorial 18

findFactorial 120
findFactorial -120
