﻿//// Body is <is full word?> * <child nodes>
//
//type TreeNode = { IsFullWord : bool; Children : System.Collections.Generic.Dictionary<char, TreeNode> }
//
//type ChildNodes = System.Collections.Generic.Dictionary<char, TreeNode>
//
//let a = { IsFullWord = false; Children = new System.Collections.Generic.Dictionary<char, TreeNode>() }
//
//let an = { IsFullWord = false; Children = new System.Collections.Generic.Dictionary<char, TreeNode>() }
//
//let ant = { IsFullWord = true; Children = new System.Collections.Generic.Dictionary<char, TreeNode>() }
//
//an.Children.Add('t', ant)
//a.Children.Add('n', an)
//
//let parent = new System.Collections.Generic.Dictionary<char, TreeNode>()
//parent.Add('a', a)
//
//let originalWord = [| 't'; 'a'; 'n' |]
//
//
//let rec findMatches (remainingLetters : array<char>) (currentLetter : TreeNode) (matches : Set<string>) = 
//
//    if (remainingLetters.Length = 0) then currentLetter.IsFullWord
//    else
//        let lettersWithMatch =
//            remainingLetters
//            |> Array.filter(currentLetter.Children.ContainsKey)
//            |> Array.distinct
//
//        lettersWithMatch
//        |> Array.map()
//
//        currentLetter.IsFullWord

