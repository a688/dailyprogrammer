﻿// Find every possible combination for a word and weigh each answer:
//  Each position is weighed starting at 0 and weighted by 100 at each position.  Using 'Iron' for an example
// I = 0, r = 100, o = 200, n = 300
//
// Each letter is weighted by how far away it is from 'a' (the string is lowercased for this comparison). Using 'iron'
// for an example:
// i = i - a = 105 - 97 =  8
// r = r - a = 114 - 97 = 17
// o = o - a = 111 - 97 = 14
// n = n - a = 110 - 97 = 13
//
// The toal weight for each combination (using iron as an example) would be
// Ir: [i   ] + [ r  ] =   0 +  8 + 100 + 17 = 125
// Io: [i   ] + [  o ] =   0 +  8 + 200 + 14 = 222
// In: [i   ] + [   n] =   0 +  8 + 300 + 13 = 321
// Ro: [ r  ] + [  o ] = 100 + 17 + 200 + 14 = 331
// Rn: [ r  ] + [   n] = 100 + 17 + 300 + 13 = 430
// On: [  o ] + [   n] = 200 + 14 + 300 + 13 = 527
//
// The combination closer to 0 is the one we perfer

type Abbrev = { Initials : string; Score : int }
type Element = { Name : string; Choices : list<Abbrev>; BestChoice: option<Abbrev>}

let scoreLetter pos (letter : char) = (pos * 100) + (System.Convert.ToInt32(letter) - 97)

let scoreAbbreviations (name : string) =
    let allScores = System.Collections.Generic.List<Abbrev>()

    for x in 0..(name.Length - 2) do
        let outerScore = scoreLetter x name.[x]
        for y in (x + 1)..(name.Length - 1) do
            let innerScore = scoreLetter y name.[y]
            allScores.Add({ Initials = (name.Substring(x, 1) + name.Substring(y, 1)); Score = (outerScore + innerScore)})

    { Name = name; Choices = (allScores |> Seq.sortBy(fun x -> x.Score) |> Seq.toList); BestChoice = None }

let lowestPossibleScore (e : Element) (existing : System.Collections.Generic.Dictionary<string, Element>) lowerLimit =
    e.Choices 
    |> List.tryFind(fun x -> (existing.ContainsKey(x.Initials) = false) && (x.Score > lowerLimit))

let scoreForAbbreviation (e : Element) (abbreviation : string) =
    (e.Choices |> List.find(fun x -> x.Initials = abbreviation)).Score

let processElements (elements : list<string>) = 
    let scoredElements = 
        (elements |> List.map(fun x -> x.ToLower()) |> List.sortBy(fun x -> x.Length))
        |> List.map scoreAbbreviations

    let existing = new System.Collections.Generic.Dictionary<string, Element>()

    scoredElements
    |> List.choose(fun ele -> 
        match (lowestPossibleScore ele existing 0) with 
        | None -> Some(ele)
        | Some a -> 
            existing.Add(a.Initials, {ele with BestChoice = Some(a)})
            None
        )
    |> List.iter(fun x -> 
        let possibleChanges = 
            x.Choices 
            |> List.choose(fun y -> 
                let currentScore = scoreForAbbreviation existing.[y.Initials] y.Initials
                let nextBest = lowestPossibleScore existing.[y.Initials] existing currentScore
                    
                if nextBest.IsNone then None
                else Some(y.Initials, { existing.[y.Initials] with BestChoice = nextBest })
            )
                
        match possibleChanges with
        | head::tail -> 
            let (i, e) = head
            existing.Add(e.BestChoice.Value.Initials, e)
            existing.[i] <- x
        | _ -> ignore()
    )

    let namedElements = existing |> Seq.map(fun kvp -> kvp.Value) |> Seq.toList
    let namedNames = namedElements |> List.map(fun e -> e.Name)
    let unNamedElements = elements |> Seq.filter(fun n -> (namedNames |> List.contains(n.ToLower())) = false)

    (namedElements, unNamedElements)

let file = System.IO.File.ReadAllLines(__SOURCE_DIRECTORY__ + @"\20160713.txt") |> Array.toList
let results = processElements file
    
printfn "%i elements have no abbreviations" ((snd results) |> Seq.length)       