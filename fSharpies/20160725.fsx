﻿let getSides (equation : string) =
    equation.Split([| " " |], System.StringSplitOptions.RemoveEmptyEntries)

let rec gcd smallest largest =
    match largest with
    | 0 -> smallest
    | _ -> gcd largest (smallest % largest)
        
let processNumericEquation numerator denominator =
    match numerator, denominator with
    | _, 0 -> "UNDEFINED"
    | 0, _ -> "0"
    | _, _ ->
        let ordered = [| numerator; denominator |] |> Array.sort
        let divisor = gcd ordered.[0] ordered.[1]
        sprintf "%i / %i" (numerator / divisor) (denominator / divisor)

let getLetterCount (str : string) = 
    str.ToCharArray()
    |> Array.groupBy(fun x -> x)
    |> Array.map(fun (letter, letters) -> letter, (letters |> Array.length))

let getDistinctLetters (source : array<(char * int)>) (checkAgainst : array<(char * int)>) =
    source
    |> Array.map (fun (letter, count) -> 
        let countInDenominator = checkAgainst |> Array.filter(fun (l, _) -> l = letter) |> Array.sumBy(fun (l, c) -> c)
        (letter, count - countInDenominator)
        )
    |> Array.filter(fun (letter, count) -> count > 0)
    |> Array.fold(fun acc (l, c) -> acc + new System.String(l, c)) ""

let oneIfBlank value =
    match System.String.IsNullOrWhiteSpace(value) with
    | true -> "1"
    | _ -> value

let processAlphaEquation (numerator : string) (denominator : string) =
    let uniqueN = getLetterCount numerator
    let uniqueD = getLetterCount denominator

    let goodNum = getDistinctLetters uniqueN uniqueD
    let goodDen = getDistinctLetters uniqueD uniqueN

    sprintf "%s / %s" (oneIfBlank goodNum) (oneIfBlank goodDen)

let processValidEquation numerator denominator = 
    let parsedNum, num = System.Int32.TryParse(numerator)
    let parsedDen, den = System.Int32.TryParse(denominator)

    match parsedNum, parsedDen with
    | true, true -> processNumericEquation num den
    | _, _ -> processAlphaEquation numerator denominator

let processEquation equation =
    let sides = getSides equation

    match sides.Length with
    | x when x = 2 -> processValidEquation sides.[0] sides.[1]
    | _ -> "Invalid equation"
    
printfn "%s " (processEquation "4 8")
printfn "%s " (processEquation "1536 78360")
printfn "%s " (processEquation "51478 5536")
printfn "%s " (processEquation "46410 119340")
printfn "%s " (processEquation "7673 4729")
printfn "%s " (processEquation "4096 1024")

printfn "%s " (processEquation "ab cb")
printfn "%s " (processEquation "ab x")
printfn "%s " (processEquation "a a")
