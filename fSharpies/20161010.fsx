﻿let descending = fun acc (x : char) -> acc + x.ToString()
let ascending = fun acc (x : char) -> x.ToString() + acc

let sortedDigits digits folder = 
    let rec make4 (acc : string) = 
        match acc.Length with
        | x when x >= 4 -> acc.Substring(0, 4)
        | _ -> make4 (acc + "0")

    (make4 digits) |> Seq.sortByDescending(id) |> Seq.fold(folder) ""

let desc_digits digits = sortedDigits digits descending
let largest_digit digits = (desc_digits digits).Substring(0, 1)

let kaprekar digits = 
    let rec internalKaprekar digits depthCounter =
        let desc = System.Int32.Parse(sortedDigits digits descending)
        let ascd = System.Int32.Parse(sortedDigits digits ascending)

        match (desc = ascd), digits with
        | true, _ -> "INVALID"
        | _, x when x = "6174" -> depthCounter.ToString()
        | _ -> internalKaprekar ((desc - ascd).ToString()) (depthCounter + 1)
    internalKaprekar digits 0
    

largest_digit("1234");;     // 4
largest_digit("3253");;     // 5
largest_digit("9800");;     // 9
largest_digit("3333");;     // 3
largest_digit("120");;      // 2

desc_digits("1234");;       // 4321
desc_digits("3253");;       // 5332
desc_digits("9800");;       // 9800
desc_digits("3333");;       // 3333
desc_digits("120");;        // 2100

kaprekar("6589");;          // 2
kaprekar("5455");;          // 5
kaprekar("6174");;          // 0