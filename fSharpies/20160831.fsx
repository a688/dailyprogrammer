﻿type WordMatch (fn : string, ln : string) = 
    member x.Fn = fn
    member x.Ln = ln
    member x.WholeWord = x.Fn + x.Ln
    member x.Value = (x.Fn.Length * x.Fn.Length) + (x.Ln.Length * x.Ln.Length)

let readInDictionary (wordFile : string) =
    let dict = new System.Collections.Generic.HashSet<string>()

    System.IO.File.ReadAllLines(wordFile)
    |> Array.iter(fun x -> 
        match System.String.IsNullOrWhiteSpace(x) with
        | false -> dict.Add(x) |> ignore
        | true -> ignore()
        )

    dict

let rec buildRemainder (remaining : list<string>) (acc1 : string) (acc2 : list<string>) =
        match remaining with
        | [] -> acc2 |> List.distinct
        | head::tail -> buildRemainder tail (acc1 + head) ([ acc1 + head ] |> List.append acc2)

let breakDownName (remaining : list<string>) =
    let rec buildParts (remaining : list<string>) (acc : list<string>)=
        match remaining with
        | [] -> (remaining |> List.map(fun x -> x.ToString())) |> List.append acc
        | head::tail -> 
            let buildTail = buildRemainder tail head [ head ]
            buildParts tail (buildTail |> List.append acc)

    (buildParts remaining [])
    |> List.distinct
    |> List.sortByDescending(fun x -> x.Length)

let findMatches (firstName : list<string>) (lastName : list<string>) (dict : System.Collections.Generic.HashSet<string>) =
    firstName
    |> List.map(fun fn ->
        lastName
        |> List.choose(fun ln -> 
            match dict.Contains(fn + ln) with
            | false -> None
            | true -> Some(WordMatch(fn, ln))
            )
        )
    |> List.collect(id)
    |> List.sortByDescending(fun x -> x.Value)

let processName (firstName : string) (lastName : string) (parserType : int) (dict : System.Collections.Generic.HashSet<string>) =
    let tmpFName = firstName.ToLower() |> Seq.map(string) |> Seq.toList
    let fName = 
        match parserType with 
        | 0 -> breakDownName tmpFName
        | _ -> buildRemainder tmpFName "" [ tmpFName.[0] ]
        
    let lName = breakDownName (lastName.ToLower() |> Seq.map(string) |> Seq.toList)

    let matches = findMatches fName lName dict

    match matches.Length with
    | 0 -> "NO MATCHES FOUND"
    | _ -> sprintf "%s (%i)" matches.Head.WholeWord matches.Head.Value

let dict = readInDictionary @"C:\Users\jalon\Downloads\enable1.txt"

printfn "Ada Lovelace: %s" (processName "Ada" "Lovelace" 0 dict)
printfn "Haskell Curry: %s" (processName "Haskell" "Curry" 0 dict)
printfn "Donald Knuth: %s" (processName "Donald" "Knuth" 0 dict)
printfn "Alan Turing: %s" (processName "Alan" "Turing" 0 dict)
printfn "Claude Shannon: %s" (processName "Claude" "Shannon" 0 dict)