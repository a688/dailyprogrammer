﻿// https://www.reddit.com/r/dailyprogrammer/comments/56b3ga/20161007_challenge_286_hard_nonogram_solver/?st=itzsqqh9&sh=a46db6ae
let possibleRowSolutions (counts : string) maxWidth =
    let individualCounts =  counts.Split([| " " |], System.StringSplitOptions.RemoveEmptyEntries) |> List.ofArray
    let limitChecker = 1 <<< maxWidth
    
    // Builds a single block of bits
    let rec buildBlock width block =
        match width with 
        | x when x = 0 -> block
        | _ -> buildBlock (width - 1) ((block <<< 1) ||| 1)

    // Generates all possible solutions of a given line by shifting the line left by one bit until 
    // the limit has been reached
    let rec buildAllShiftedPositions currentLine solutions =
        match (currentLine > limitChecker) with
        | true -> solutions
        | false -> buildAllShiftedPositions (currentLine << 1) (currentLine :: solutions)

    // Adds a block in all possible positions of the original currentLine
    let rec addBlock block currentLine solutions =
        match (currentLine > limitChecker) with
        | true -> solutions
        | false -> 
            let validSolutions = buildAllShiftedPositions (currentLine ||| block) [ ]
            addBlock block (currentLine <<< 1) (validSolutions : solutions)
   
    let rec buildBlocks countList spaceCount currentLine solutions =
        match countList with
        | [] -> Some (solutions)        // All possible solutions found
        | head::tail -> 
            let 
            if (builder > limitChecker) then None
            
