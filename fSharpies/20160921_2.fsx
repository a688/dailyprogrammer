﻿let template alphabet = 
    [| 
        "     ________________________________________________________________";
        ("    /" + alphabet);
        "12 / O           OOOOOOOOO                        OOOOOO             ";
        "11|   O                   OOOOOOOOO                     OOOOOO       ";
        " 0|    O                           OOOOOOOOO                  OOOOOO ";
        " 1|     O        O        O        O                                 ";
        " 2|      O        O        O        O       O     O     O     O      ";
        " 3|       O        O        O        O       O     O     O     O     ";
        " 4|        O        O        O        O       O     O     O     O    ";
        " 5|         O        O        O        O       O     O     O     O   ";
        " 6|          O        O        O        O       O     O     O     O  ";
        " 7|           O        O        O        O       O     O     O     O ";
        " 8|            O        O        O        O OOOOOOOOOOOOOOOOOOOOOOOO ";
        " 9|             O        O        O        O                         ";
        "  |__________________________________________________________________"
    |] |> Array.map(fun line -> line |> Seq.map(fun x -> x.ToString()) |> Seq.toArray)

let getEncoding (template : string[][]) (col : int) =
    (Array.init 15 (fun x -> template.[x].[col])) |> Array.reduce(+)

let encodePhrase (phrase : string) (template : string[][]) (lookup : Map<char, int>) =
    phrase |> Seq.map(fun l -> getEncoding template lookup.[l]) |> Array.ofSeq

let cardStart (template : string[][]) =
    seq { for x in 0..4 do yield (getEncoding template x) } |> Array.ofSeq

let printAnswer (data : array<string>) =
    Array.init data.[0].Length (fun x -> 
        Array.init data.Length (fun y -> data.[y].[x].ToString()) |> Array.reduce(+)
    )
    |> Array.iter(fun l -> printfn "%s" l)

let printCard letters (word : string) =
    let cardTemplate = template letters
    let letterLookup = letters |> Seq.mapi(fun i c -> (c, i + 5)) |> Map.ofSeq
    printAnswer (Array.append (cardStart cardTemplate) (encodePhrase (word.ToUpper()) cardTemplate letterLookup))

printCard "&-0123456789ABCDEFGHIJKLMNOPQR/STUVWXYZ:#@'=\"[.<(+^!$*);\],%_>? " "Hello World"