﻿let punchCharacter = "0"
let letterSpot = "@"

let rawTemplate = 
    [| 
        4613933420194820096I;
        2305847398670274496I;
        1152921513180004415I;
        577588855528488960I;
        288794427772766240I;
        144397213886383120I;
        72198606943191560I;
        36099303471595780I;
        18049651735797890I;
        9024825867898945I;
        4512412950593535I;
        2256206466908160I
    |]

let maskTemplate =
    Array.init 63 (fun x -> 
        List.init 12 (fun y -> 
            if ((rawTemplate.[y] >>> x) &&& 1I) = 1I then punchCharacter 
            else " "
        )  |> List.reduce(+)
    )
    |> Array.map(fun text -> "_" + letterSpot + text + "_")

let startCard = 
    [ 
        "  11           "; 
        "  210123456789 "; 
        "   ||||||||||||"; 
        "  /           _"; 
        " /            _" 
    ]

let printAnswer (data : list<string>) =
    Array.init data.[0].Length (fun x -> 
        Array.init data.Length (fun y -> data.[y].[x].ToString()) |> Array.reduce(+)
    )
    |> Array.iter(fun l -> printfn "%s" l)

let processCode (lookup : Map<char,int>) (word : string) =
    let punches = 
        word.ToUpper() 
        |> Seq.map(fun c -> 
            if (c = ' ') then "_             _"
            else maskTemplate.[lookup.[c]].Replace(letterSpot, c.ToString())
        ) 
        |> List.ofSeq
    printAnswer (List.append startCard punches)

let cardDictionary = "&-0123456789ABCDEFGHIJKLMNOPQR/STUVWXYZ:#@'=\"[.<(+^!$*);\],%_>?" |> Seq.rev |> Seq.mapi(fun i c -> (c, i)) |> Map.ofSeq

processCode cardDictionary "&-0123456789ABCDEFGHIJKLMNOPQR/STUVWXYZ:#@'=\"[.<(+^!$*);\],%_>?"
processCode cardDictionary "Hello, world!"
processCode cardDictionary "This is Reddit's r/dailyprogrammer challenge."
processCode cardDictionary "WRITE (6,7) FORMAT(13H HELLO, WORLD) STOP END"