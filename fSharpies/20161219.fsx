﻿let days = [| "first"; "second"; "third"; "fourth"; "fifth"; "sixth"; "seventh"; "eighth"; "ninth"; "tenth"; "eleventh"; "twelth" |]
let nbrs = [| "1"; "2"; "3"; "4"; "5"; "6"; "7"; "8"; "9"; "10"; "11"; "12" |]
let words = [| "a"; "two"; "three"; "four"; "five"; "six"; "seven"; "eigth"; "nine"; "ten"; "eleven"; "twelve" |]
    
let phrase day = sprintf "On the %s day of Christmas\r\nmy true love sent to me:\r\n" day
let buildLine count item = sprintf "%s %s\r\n" count item
    
let singSong (lines : list<string>) (countSource : array<string>) =
    let lastLine = buildLine countSource.[0] lines.[0]
    
    let rec singIt (linesRemaining : list<string>) (day : int) (refrain : string) =
        match linesRemaining with 
        | [] -> printfn ""
        | head::tail ->
            let line = buildLine countSource.[day] head
            printfn "%s%s%s" (phrase days.[day]) line refrain
            singIt tail (day + 1) (line + refrain)
    
    printfn "%s%s" (phrase days.[0]) lastLine
    singIt (lines |> List.tail) 1 (sprintf "and %s" lastLine)
    
let lines = [
    "Partridge in a Pear Tree"; 
    "Turtle Doves"; 
    "French Hens"; 
    "Calling Birds"; 
    "Golden Rings"; 
    "Geese a Laying"; 
    "Swans a Swimming"; 
    "Maids a Milking"; 
    "Ladies Dancing"; 
    "Lords a Leaping"; 
    "Pipers Piping"; 
    "Drummers Drumming" ] 
    
singSong lines words