﻿#r "../packages/BenchmarkDotNet.0.9.8/lib/net45/BenchmarkDotNet.dll"

open BenchmarkDotNet.Attributes
open BenchmarkDotNet.Running

type Grid = int[,]
type Cell = int * int


let getValues (puzzle : Grid) (cells : array<(Cell)>) =
    cells
    |> Array.map(fun (r, c) -> puzzle.[r,c])
    |> Array.distinct


let cellsInLocalSquare (loc : Cell) =
    let frow = [| 0; 0; 0; 3; 3; 3; 6; 6; 6 |].[fst loc]    // could do ((row / 3) * 3)
    let fcol = [| 0; 0; 0; 3; 3; 3; 6; 6; 6 |].[snd loc]    // could do ((col / 3) * 3)
    let colIncrement = [| 0; 0; 0; 1; 1; 1; 2; 2; 2 |]      // could be replaced with (idx / 3) in the Array.init section

    // This gets the cells in the order of [| (0,0); (1, 0); (2, 0); (0, 1); ... (2, 2) |]
    Array.init 9 (fun idx -> (frow + (idx % 3), fcol + colIncrement.[idx]))


let relatedCells (loc : Cell) =
    let cache = new System.Collections.Generic.Dictionary<string, array<Cell>>()

    let findRelatedCells loc =
        let key = (fst loc).ToString() + "_" + (snd loc).ToString()
        if (not (cache.ContainsKey(key))) then 
            cache.Add(key, 
                (cellsInLocalSquare loc)
                |> Array.append(Array.init 9 (fun x -> (x, snd loc)))
                |> Array.append(Array.init 9 (fun x -> (fst loc, x)))
            )
        cache.[key]

    findRelatedCells loc

let possibleValues (loc : Cell) (puzzle : Grid) =
    // Get all known values in this same row and column
    let unavailable = getValues puzzle (relatedCells loc)

    // Figure out which possible values are remaining in this cell
    [| 1; 2; 3; 4; 5; 6; 7; 8; 9 |]
    |> Array.filter(fun x -> x > puzzle.[fst loc, snd loc])                     // Only look at values GREATER than the current value
    |> Array.filter(fun x -> not (unavailable |> Array.contains(x)))            // Only get values that are not already in this row/column


let updatedPuzzle (puzzle : Grid) (loc : Cell) value =
    let updated = Array2D.copy puzzle
    updated.[fst loc, snd loc] <- value
    updated


let nextCell (row, col) =
    match col with 
    | x when x = 8 -> (row + 1, 0)
    | _ -> (row, col + 1)


let rec solver (loc : Cell) (puzzle : Grid) =
    match (fst loc), (snd loc) with
    | 9, _ -> puzzle, true                              // If we are here then we are at the end of the puzzle
    | r, c when puzzle.[r,c] = 0 -> 
        let solution = 
            (possibleValues loc puzzle)
            |> Array.tryPick(fun v ->
                let solution, solved = solver (nextCell loc) (updatedPuzzle puzzle loc v)
                match solved with
                | true -> Some (solution, true)
                | false -> None
                )
        match solution with
        | None -> (puzzle, false)
        | Some(p, c) -> (p, c)
    | _ -> solver (nextCell loc) puzzle                 // If this cell already has a value then it IS correct already


let rec solver2 (loc : Cell) (puzzle : Grid) =
    match (fst loc), (snd loc) with
    | 9, _ -> puzzle, true                              // If we are here then we are at the end of the puzzle
    | r, c when puzzle.[r,c] = 0 -> 
        (possibleValues loc puzzle)
        |> Array.map(fun v -> solver2 (nextCell loc) (updatedPuzzle puzzle loc v))
        |> Array.fold(fun acc (p, solved) -> 
            if (solved) then (p, solved)
            else acc) (puzzle, false)
    | _ -> solver2 (nextCell loc) puzzle                // If this cell already has a value then it IS correct already

let printGrid (puzzle : Grid) =
    for r in 0..8 do
        for c in 0..8 do
            printf "%i " puzzle.[r,c]
        printfn ""

[<Benchmark>]
let solvePuzzle (puzzle : int[,]) =
    let solution, solved = puzzle |> solver (0, 0)
    printGrid solution
    solved

[<Benchmark>]
let solvePuzzle2 (puzzle : int[,]) =
    let solution, solved = puzzle |> solver2 (0, 0)
    printGrid solution
    solved

let createGrid =
    let tmpGrid = [|
        [| 5; 3; 0; 0; 7; 0; 0; 0; 0 |]
        [| 6; 0; 0; 1; 9; 5; 0; 0; 0 |]
        [| 0; 9; 8; 0; 0; 0; 0; 6; 0 |]
        [| 8; 0; 0; 0; 6; 0; 0; 0; 3 |]
        [| 4; 0; 0; 8; 0; 3; 0; 0; 1 |]
        [| 7; 0; 0; 0; 2; 0; 0; 0; 6 |]
        [| 0; 6; 0; 0; 0; 0; 2; 8; 0 |]
        [| 0; 0; 0; 4; 1; 9; 0; 0; 5 |]
        [| 0; 0; 0; 0; 8; 0; 0; 7; 9 |]
        |]
    
    Array2D.init 9 9 (fun i j -> tmpGrid.[i].[j])

let doIt = solvePuzzle createGrid
let doIt2 = solvePuzzle2 createGrid