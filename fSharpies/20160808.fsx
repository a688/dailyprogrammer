﻿//type ProcessType =
//    | Bracket
//    | Weave
//
//let determineProcessType (definition : string) =
//    match System.String.Equals(definition, "Weave", System.StringComparison.OrdinalIgnoreCase) with
//    | true -> ProcessType.Weave
//    | _ -> ProcessType.Bracket

//let groupByOne (value : string) =
//    match value.Length with
//    | x when x = 1 -> [| value |]
//    | _ ->
//        let rec internalSplit (remaining : string) (builtUp : array<string>) =
//            match remaining.Length with
//            | x when x = 0 -> builtUp
//            | x when x = 1 -> (builtUp |> Array.append [| remaining.Substring(0, 1) |])
//            | _ -> internalSplit remaining.[1..] (builtUp |> Array.append [| remaining.Substring(0, 1) |])
//            
//        internalSplit value [| |] |> Array.rev
//
//let groupByTwo (value : string) =
//    match value.Length with
//    | x when x < 3 -> [| value |]
//    | _ ->
//        let rec internalSplit (remaining : string) (builtUp : array<string>) =
//            match remaining.Length with
//            | x when x <= 2 -> [| remaining |] |> Array.append builtUp
//            | _ -> internalSplit remaining.[2..] ([| remaining.[0..1] |] |> Array.append builtUp)
//
//        internalSplit value [| |]


let rec fixArrayCyclically (source : array<string>) (necessaryLength : int) =
    match (source |> Array.length) with
    | x when x = necessaryLength -> source
    | x when x > necessaryLength -> source.[0..necessaryLength - 1]
    | _ -> fixArrayCyclically (source |> Array.append source) necessaryLength

let weaveInput (a : array<string>) (b : array<string>) =
    let fixedA = [| " " |] |> Array.append (fixArrayCyclically a (b.Length - 1))
    
    fixedA
    |> Array.zip(b)
    |> Array.map(fun (x, y) -> 
        match System.String.IsNullOrWhiteSpace(y) with
        | true -> [| x |]
        | false -> [| x; y; |]
        )
    |> Array.reduce (fun acc elem -> elem |> Array.append acc)

let bracketInput (a : array<string>) (b : array<string>) =
    let longest = [| a.Length; b.Length |] |> Array.max

    let fixedA = fixArrayCyclically a longest
    let fixedb = fixArrayCyclically b longest
    
    fixedb
    |> Array.zip(fixedA)
    |> Array.map(fun (y, x) -> [| x.Substring(0, 1) + y + x.Substring(1, 1) |])
    |> Array.reduce (fun acc elem -> elem |> Array.append acc)

//    let parts = "Bracket\r\n+-\r\n234567".Split([| "\r\n" |], System.StringSplitOptions.RemoveEmptyEntries)
//
//    let result =
//        match (determineProcessType(parts.[0])) with
//        | ProcessType.Bracket -> (bracketInput (groupByOne parts.[1]) (groupByTwo parts.[2]))
//        | ProcessType.Weave -> (weaveInput (groupByOne parts.[1]) (groupByOne parts.[2]))

//    let parts = "Bracket\r\n+-\r\n234567".Split([| "\r\n" |], System.StringSplitOptions.RemoveEmptyEntries)

let r1 = (bracketInput [| "+"; "-" |] [| "23"; "45"; "67" |])
printfn "Result 1: %A" r1
let r2 =(bracketInput r1 [| "()" |])
printfn "Result 2: %A" r2
printfn "Result 3: %A" (weaveInput [| "*" |] r2)

