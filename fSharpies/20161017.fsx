﻿let skipWords = new System.Collections.Generic.HashSet<string>([| "I"; "a"; "about"; "an"; "and"; "are"; "as"; "at"; "be"; "by"; "com"; "for"; "from"; "how"; "in"; "is"; "it"; "of"; "on"; "or"; "that"; "the"; "this"; "to"; "was"; "what"; "when"; "where"; "who"; "will"; "with"; "the" |])

let wordsInPhrase (phrase : string) = 
    phrase.ToLower().Split([| " "; "\""; ","; "."; "?" |], System.StringSplitOptions.RemoveEmptyEntries)
    |> Array.filter(skipWords.Contains >> not)
    |> List.ofArray

let findAlliterations phrase =
    let rec buildListing (remaining : list<string>) currentLetter (currentSet : list<string>) (allSets : list<list<string>>) =
        match remaining, (currentSet.Length) with
        | [], x when x < 2 -> allSets
        | [], _ -> currentSet :: allSets
        | head::tail, _ -> 
            match (head.[0] = currentLetter), (currentSet.Length) with
            | true, _  -> buildListing tail currentLetter (head :: currentSet) allSets
            | false, x when x < 2 -> buildListing tail head.[0] [ head ] allSets
            | false, _ -> buildListing tail head.[0] [ head ] (currentSet :: allSets)

    (buildListing (wordsInPhrase phrase) ' ' [] [])
    |> List.groupBy(fun il -> il.[0].[0])
    |> List.map(fun (l, lofl) -> lofl |> List.collect(id) |> List.distinct |> List.reduce(fun a b -> b + " " + a))

findAlliterations "The daily diary of the American dream";;
findAlliterations "For the sky and the sea, and the sea and the sky";;
findAlliterations "Three grey geese in a green field grazing, Grey were the geese and green was the grazing.";;
findAlliterations "But a better butter makes a batter better.";;
findAlliterations "\"His soul swooned slowly as he heard the snow falling faintly through the universe and faintly falling, like the descent of their last end, upon all the living and the dead.\"";;
findAlliterations "Whisper words of wisdom, let it be.";;
findAlliterations "They paved paradise and put up a parking lot.";;
findAlliterations "So what we gonna have, dessert or disaster?";;
