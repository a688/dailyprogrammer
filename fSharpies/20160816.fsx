﻿let shifts = [| 18; 12; 6; 0 |]

let encodeByte value shift = (char)(((value >>> shift) &&& 0x3F) + 32)
let encodeBlock (arr : array<int>) =
    let value = (arr.[0] <<< 16) ||| (arr.[1] <<< 8) ||| arr.[2]
    new System.String( shifts |> Array.map(fun x -> encodeByte value x) )

let decodeByte value shift = (((int)value - 32) &&& 0x3F) <<< (shift)
let decodeBlock (data : string) =
    let toShift = (decodeByte data.[0] shifts.[0]) ||| (decodeByte data.[1] shifts.[1]) ||| (decodeByte data.[2] shifts.[2]) ||| (decodeByte data.[3] shifts.[3])
    [| (byte)(toShift >>> 16); (byte)((toShift >>> 8) &&& 0xFF); (byte)(toShift &&& 0xFF) |]


let safeChunker (data : array<'T>) (size : int) =
    match (data.Length % size) with
    | x when x = 0 -> data |> Array.chunkBySize size
    | x -> 
        (Array.zeroCreate (size - x))
        |> Array.append(data)
        |> Array.chunkBySize size

let splitIntoLines (text : string) = 
    seq { for x in 0..(text.Length - 1) do yield text.[x] } 
    |> Seq.chunkBySize(45) 
    |> Seq.map(fun x -> new System.String(x))


let encodeAndAlign (input : string) =
    safeChunker (System.Text.Encoding.ASCII.GetBytes(input) |> Array.map(System.Convert.ToInt32)) 3

let decodeAndAlign (input : string) =
    (safeChunker (seq { for x in 0..(input.Length - 1) do yield (byte)input.[x]} |> Seq.toArray) 4)
    |> Array.map(fun x -> System.Text.Encoding.ASCII.GetString(x))


let encodeLine (line : string) =
    let encodedText = 
        (encodeAndAlign line)
        |> Array.map(encodeBlock)
        |> Array.reduce(+)
    ((char)(line.Length + 32)).ToString() + encodedText

let decodeLine (line : string) =
    match (line.[0] = '`') with
    | true -> ""
    | _ ->
        System.Text.Encoding.ASCII.GetString(
            (decodeAndAlign (line.Substring(1)))
            |> Array.map(decodeBlock)
            |> Array.collect(fun x -> x)
        )

let uuEncodeText (input : string) =
    (splitIntoLines input)
    |> Seq.map(encodeLine)

let uuDecodeText (input : string) (lineSeperator : string) =
    input.Split([| lineSeperator |], System.StringSplitOptions.RemoveEmptyEntries)
    |> Array.map(decodeLine)
    |> String.concat ""
    
let uuEncodeToFile (text : string) (filePath : string) =
    let lines = (uuEncodeText text)

    use stream = new System.IO.FileStream(filePath, System.IO.FileMode.Create, System.IO.FileAccess.Write)
    use writer = new System.IO.StreamWriter(stream)

    writer.WriteLine("begin 644 " + System.IO.Path.GetFileName(filePath))
    lines |> Seq.iter(fun x -> writer.WriteLine(x))
    writer.Write("`" + System.Environment.NewLine + "end")

let uuDecodeFile (filePath : string) = 
    ""

let encodedAnswer = encodeBlock [| 67; 97; 116 |]
let telephoneGame = decodeBlock encodedAnswer
printfn "%A" telephoneGame

let encodedLine = encodeLine "Cat"
let decodedLine = decodeLine encodedLine
printfn "%s" decodedLine