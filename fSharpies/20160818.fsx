﻿let rec breakDownWord word length =
    let rec internalWorker (remaining : string) (final : string) =
        match remaining.Length with
        | x when x <= length -> final + remaining
        | _ -> 
            let word = remaining.Substring(0, length - 1) + "-"
            internalWorker (remaining.Substring(length - 1)) (final + word + "\r\n")

    internalWorker word ""

let adjustLine (line : string) (width : int) =
    if (line.Length < width) && (line.IndexOf(" ") = -1) then line
    elif (line.Length > width) then line
    else
        let indexes =  seq { for x = 0 to (line.Length - 1) do if line.[x] = ' ' then yield x } |> Seq.toArray
        let arrCnt = indexes |> Array.length

        let incr indexes (indexUpdated : int) =
            indexes |> Array.mapi(fun i x -> 
                if i >= indexUpdated then x + 1
                else x)

        let rec adjuster (indexes : array<int>) (x : int) (output : string) =
            match output.Length >= width with
            | true -> output
            | _ -> 
                adjuster (incr indexes (x % arrCnt)) (x + 1) (output.Insert(indexes.[x % arrCnt], " "))

        adjuster indexes 0 line

let beautify (inputText : string) lineWidth =
    inputText.Split([| "\r\n" |], System.StringSplitOptions.RemoveEmptyEntries)
    |> Array.fold(fun acc l -> acc + (adjustLine l lineWidth) + "\r\n") ""

let split (inputText : string) lineWidth =
    let seperateWords = inputText.Replace("\r\n", " \r\n ").Split([| " " |], System.StringSplitOptions.RemoveEmptyEntries) |> Array.toList

    let rec buildOutput (words : list<string>) (line : string) (acc : string) =
        match words with 
        | [] -> acc + line
        | head::tail -> 
            match head, head.Length, line.Length with
            | h, _, _ when h = "\r\n" -> buildOutput tail "" (acc + line + "\r\n")
            | _, hl, ll when hl + ll < lineWidth -> buildOutput tail ((line + " " + head).Trim()) acc
            | _, hl, ll when (hl > lineWidth) && (ll > 0) -> buildOutput tail "" (acc + line + "\r\n" + (breakDownWord head lineWidth))
            | _, hl, ll when (hl > lineWidth) && (ll = 0) -> buildOutput tail "" (acc + (breakDownWord head lineWidth))
            | _ -> buildOutput tail head (acc + line + "\r\n")

    buildOutput seperateWords "" ""


let inputText = 
    "In the beginning God created the heavens and the earth. Now the earth was formless and empty, darkness was over the surface of the deep, and the Spirit of God was hovering over the waters.\r\n\r\n" +
    "And God said, \"Let there be light,\" and there was light. God saw that the light was good, and he separated the light from the darkness. God called the light \"day,\" and the darkness he called \"night.\" And there was evening, and there was morning - the first day."