﻿type SinglePeriod = { Period : System.DateTime; InterestExpense : float; Amortization : float; PresentValue : float }

/// <summary>
/// Calculate the present value of a loan
/// </summary>
/// <param name="rate">Rate of the loan in decimal form (eg: 8.00% would be 0.08)</param>
/// <param name="numberOfPaymentPeriods">Number of payment periods until maturity</param>
/// <param name="futureValue">Future value of the loan</param>
let calculatePresentValue rate numberOfPaymentPeriods futureValue =
    futureValue * (1.0 / System.Math.Pow(1.0 + rate, numberOfPaymentPeriods))

let calcPrice couponPayment ytm faceValue numberOfPayments = 
    let ytmToPayments = System.Math.Pow(1.0 + ytm, numberOfPayments)
    (couponPayment * (((1.0 - (1.0 / ytmToPayments)) / ytm) + faceValue)) / ytmToPayments

/// <summary>
/// Calculate the current yield of a security given its annual coupon payment and its current price
/// </summary>
/// <param name="annualInterestAmount">Annual coupon payment (eg: A $1000 bond paying 10% would pay out $100 per annum)</param>
/// <param name="currentPrice">Current price of the securtity</param>
let calculateCurrentYield annualInterestAmount currentPrice = 
    annualInterestAmount / currentPrice

let calculateZeroCouponBond period0 coupon numberOfPaymentPeriods =
    let rec calculatePeriods nbrPeriodsRemaining priorPeriod allPeriods =
        match nbrPeriodsRemaining with
        | x when x = 0 -> allPeriods
        | _ -> 
            let interestExpense = priorPeriod.PresentValue * coupon
            let currentPeriod = { 
                Period = priorPeriod.Period.AddYears(1);  
                InterestExpense = interestExpense; 
                Amortization = interestExpense * -1.0; 
                PresentValue = priorPeriod.PresentValue + interestExpense
                }
            calculatePeriods (nbrPeriodsRemaining - 1) currentPeriod (currentPeriod :: allPeriods)
    calculatePeriods numberOfPaymentPeriods period0 [ period0 ]

let monthsBetweenPayments payFrequency =
    match payFrequency with
    | 1 -> 12
    | 2 -> 6
    | 3 -> 4
    | 4 -> 3
    | 6 -> 2
    | 12 -> 1
    | _ -> failwith (sprintf "%i is an invalid payment frequency" payFrequency)

/// <summary>
/// Used to calculate the Yield to Maturity when 
/// </summary>
/// <param name="coupon"></param>
/// <param name="faceValue"></param>
/// <param name="price"></param>
/// <param name="yearsToMaturity"></param>
let calculateYieldToMaturity annualCouponPayment faceValue price yearsToMaturity = 
    (annualCouponPayment + ((faceValue - price) / yearsToMaturity)) / ((faceValue + price) / 2.0)

[<EntryPoint>]
let main argv = 
    printfn "%A" argv

    let purchasePrice = 92.0
    let faceValue = 100.0
    let coupon = 0.10
    let yearsToMaturity = 10.0

    let answer2 = calculateYieldToMaturity (faceValue * coupon) faceValue purchasePrice yearsToMaturity
    let ans3 = calcPrice (faceValue * coupon) answer2 faceValue yearsToMaturity
    printfn "%f vs %f" answer2 ans3


    0 // return an integer exit code
